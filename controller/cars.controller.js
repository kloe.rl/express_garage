// import du module express
const express = require('express');
// création d'un gestionaires de routes modulaires
const router = express.Router();

// import du module de connexion à la BDD
const connection = require('../conf/db.js');

//HTTP REQUEST METHODS

// Récupération et affichage de l'API grâce à une route GET
router.get('/', (req, res) => {
    // Execution d'une requête SQL
    connection.query('SELECT * FROM car', (err, results) => {
        // Gestion des erreurs possibles pendant l'exécution de la requête
        if (err) {
            res.status(500).send('Erreur lors de la récupération des livres');
        } else {
            // Envoi des résultats sous format JSON
            res.json(results);
        }
    })
})

//Récupération et affichage d'un objet grâce à son id grâce à une route GET
router.get('/:id', (req, res) => {
    const id = req.params.id;
    // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
    connection.execute('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération de la voiture');
        } else {
            res.json(results);
        }
    });
});

//Récupération et affichage des objets grâce à leur 'brand'
router.get('/brand/:brand', (req, res) => {
    const brand = req.params.brand;
    //
    connection.execute('SELECT * FROM car WHERE brand = ?', [brand], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de la récupération des voitures')
        } else {
            res.json(results);
        }
    });
});

// Ajout d'un nouvel objet dans la BDD grâce à la route POST
router.post('/', (req, res) => {
    const { brand, model } = req.body;
    //Exécution d'une requête SQL qui permet d'insérer un nouvel objet
    connection.execute('INSERT INTO car (brand, model) VALUES (?, ?)', [brand, model], (err, results) => {
        if (err) {
            res.status(500).send('Erreur lors de l\'ajout du la voiture');
        } else {
            //Confirmation de l'ajout de l'objet avec l'id de l'objet inséré
            res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
        }
    });
});

// Mise à jour d'un objet existant par son ID grâce à une route PUT
router.put('/:id', (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    // Exécution d'une requête pou mettre à jour les information d'un objet spécifique
    connection.execute('UPDATE car SET brand = ?, model = ? WHERE car_id = ?', [brand, model, id], (err) => {
        if (err) {
            res.status(500).send('Erreur lors de la mise à jour de la voiture');
        } else {
            // Confirmation de la mise à jour de l'objet
            res.send('Voiture mise à jour.');
        }
    });
});

// Suppression d'un objet à partir d'un ID grâce à une route DELETE
router.delete('/:id', (req, res) => {
    const id = req.params.id;
    //
    connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err) => {
        if (err) {
            res.Status(500).send('Erreur lors de la suppression de la voiture');
        } else {
            //Confirmation de la suppression 
            res.send('Voiture supprimée');
        }
    });
});

// export du module 
module.exports = router;