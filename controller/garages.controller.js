// import modle express
const express = require('express');
// creation du gestionnaire des routes modulaires
const router = express.Router();
// Import du middleware validatorGarageData
const { validatorGarageData } = require('../middleware/validator.middleware')

// import du module de connexion
const connection = require('../conf/db.js');

router.get('/', (req, res) => {
    connection.query('SELECT * FROM garage', (err, results) => {
        if (err) {
            res.status(500).json({ error: err.message })
        } else {
            res.json(results)
        }
    });
});

// ENDPOINT POST pour créer un garage
router.post('/', validatorGarageData, (req, res) => {
    const { name, email } = req.body;
    connection.execute('INSERT INTO garage (name, email) VALUES (?, ?)', [name, email], (err, results) => {
        if (err) {
            res.status(500).json(`Erreur lors de l\'ajout du garage`)
        } else {
            // res.status(201).send(`Garage ajouté avec l'ID ${results.insertId}`);
            res.status(201).json({ garage_id: results.insertId, ...req.body })
        }
    });
});

// ENDPOINT PUT pour modifier un garage
router.put('/:id', validatorGarageData, (req, res) => {
    // Récupération de l'id passé en paramètre de l'url
    const id = req.params.id;
    // Récupération des données saisies dans 'body'
    const data_garage = req.body;
    // Vérification de l'existance d'un garage spécifique
    connection.execute('SELECT * FROM garage WHERE garage_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).json({ error: err.message })
        } else {
            if (results.length === 0) {
                res.status(200).json(`Le garage ID ${id} n'existe pas`)
            } else {
                const garageToUpdate = results[0]
                console.log(garageToUpdate)
                const garageUpdated = { ...garageToUpdate, ...data_garage }
                console.log(garageUpdated)
                connection.execute('UPDATE garage SET name = ?, email = ? WHERE garage_id = ?', [garageUpdated.name, garageUpdated.email, id], (err, results) => {
                    if (err) {
                        res.status(500).json(`Erreur lors de la modification du garage ID ${id}`)
                    } else {
                        res.status(200).json(garageUpdated)
                    }
                })
            }
        }
    });
});

// ENDPOINT DELETE pour supprimer un garage
router.delete('/:id', (req, res) => {
    const id = req.params.id;
    connection.execute('DELETE FROM garage WHERE garage_id = ?', [id], (err, results) => {
        if (err) {
            res.status(500).json({ error: err.message })
        } else {
            res.status(200).json(`Le garage ID ${id} a été supprimé avec succès`)
        }
    })
})

module.exports = router;
