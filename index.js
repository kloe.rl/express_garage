// Importe et configure dotenv 
require('dotenv').config();
// Importe le module express
const express = require('express');
// Import du module CORS
const cors = require('cors');
const app = express();

// Configure l'application pour lire et interprêter les requêtes au format JSON
app.use(express.json())

// //COnfigure l'application pour lire et interprêter encodées au format URL
app.use(
    express.urlencoded({
        extended: true,
    })
);

// Import du middleware { logRequest }
const { logRequest } = require('./middleware/logger.middleware')

// Utilisation du middleware { logRequest } pour logger les requêtes
app.use(logRequest);

// Import du module qui contient toutes les routes de la table CAR
const carsController = require('./controller/cars.controller');

// Utilisation du contrôleur pour gérer les routes de la table CAR
app.use('/cars', carsController);

// Import du module qui contient les routes vers la table GARAGE
const garagesController = require('./controller/garages.controller');

// Utilisation du contrôleur pour gérer les routes de la table GARAGE
app.use('/garages', garagesController);

// Configuration qui autorise une origine spécifique (port 8080)
app.use(cors({
    origin: 'http://localhost:8080'
}));

// Défini le port qui sera écouté
const port = 3000;
//Démarrage du serveur Express et écoute du port
app.listen(port, () => {
    console.log(`Serveur en écoute sur le port ${port}`);
});