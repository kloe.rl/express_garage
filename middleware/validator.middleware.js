const validatorGarageData = (req, res, next) => {
    const { name, email } = req.body;
    // Vérifier que le nom et l'email sont présents dans la requête
    if (!name || !email) {
        return res.status(400).json({ message: 'Le nom et l\'email sont obligatoires.' });
    // condition qui permet de vérifier le bon format de l'email saisi avec une regex
    } else if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
        // si validé, passe au middleware/action suivante
        next()
    } else {
        // si non valide, renvoie un message d'erreur
        return res.status(200).json("L'adresse mail n'est pas valide")
    }
};

module.exports = { validatorGarageData }

