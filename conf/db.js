// Importe le module mysql2
const mysql2 = require('mysql2');

// Configuration & création d'un pool de connexions à la BDD 
const connection = mysql2.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD
});

// Etablissement de la connexion
connection.getConnection((err) => {
    if (err instanceof Error) {
        console.log('getConnection error:', err);
        return;
    }
});

module.exports = connection;

